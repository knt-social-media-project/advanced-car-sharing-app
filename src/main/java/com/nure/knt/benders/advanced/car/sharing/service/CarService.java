package com.nure.knt.benders.advanced.car.sharing.service;

import com.nure.knt.benders.advanced.car.sharing.dto.car.CarRequestDto;
import com.nure.knt.benders.advanced.car.sharing.dto.car.CarResponseDto;
import java.util.List;
import org.springframework.data.domain.Pageable;

public interface CarService {
    List<CarResponseDto> getAll(Pageable pageable);

    CarResponseDto getById(Long id);

    CarResponseDto add(CarRequestDto requestDto);

    CarResponseDto update(Long id, CarRequestDto requestDto);

    void delete(Long id);
}
