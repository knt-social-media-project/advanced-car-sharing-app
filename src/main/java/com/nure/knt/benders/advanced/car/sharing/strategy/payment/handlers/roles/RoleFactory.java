package com.nure.knt.benders.advanced.car.sharing.strategy.payment.handlers.roles;

import com.nure.knt.benders.advanced.car.sharing.exception.PickHandlerException;
import com.nure.knt.benders.advanced.car.sharing.strategy.payment.RoleHandler;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class RoleFactory {
    private static final String EXCEPTION_MESSAGE = "Unsupported role type";
    private final Map<String, RoleHandler> roleHandlerMap;

    public RoleHandler getRoleHandler(String roleType) {
        RoleHandler roleHandler = roleHandlerMap.get(roleType);

        if (roleHandler == null) {
            throw new PickHandlerException(EXCEPTION_MESSAGE);
        }

        return roleHandler;
    }
}
