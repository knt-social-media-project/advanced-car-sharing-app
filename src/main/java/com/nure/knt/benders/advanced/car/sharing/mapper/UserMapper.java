package com.nure.knt.benders.advanced.car.sharing.mapper;

import com.nure.knt.benders.advanced.car.sharing.config.MapperConfig;
import com.nure.knt.benders.advanced.car.sharing.dto.user.UserResponseDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.email.UserUpdateEmailResponseDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.profile.UserWithNameAndLastNameResponseDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.registration.UserRegistrationRequestDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.role.UserWithRoleResponseDto;
import com.nure.knt.benders.advanced.car.sharing.model.Role;
import com.nure.knt.benders.advanced.car.sharing.model.User;
import java.util.Set;
import java.util.stream.Collectors;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(config = MapperConfig.class)
public interface UserMapper {
    User toModel(UserRegistrationRequestDto requestDto);

    UserResponseDto toDto(User user);

    UserUpdateEmailResponseDto toDtoEmail(User user);

    UserWithNameAndLastNameResponseDto toWithNameAndLastNameResponse(User user);

    @Mapping(target = "roles", source = "user.roles", qualifiedByName = "mapRolesToIds")
    UserWithRoleResponseDto toUserWithRole(User user);

    @Named("mapRolesToIds")
    default Set<Long> mapRolesToIds(Set<Role> roles) {
        return roles.stream()
                .map(Role::getId)
                .collect(Collectors.toSet());
    }
}
