package com.nure.knt.benders.advanced.car.sharing.dto.user.email;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record UserUpdateEmailRequestDto(
        @Email
        @NotBlank
        String email
) {
}
