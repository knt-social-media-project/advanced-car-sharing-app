package com.nure.knt.benders.advanced.car.sharing.strategy.payment.handlers.type;

import com.nure.knt.benders.advanced.car.sharing.model.Rental;
import com.nure.knt.benders.advanced.car.sharing.strategy.payment.TypeHandler;
import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;
import org.springframework.stereotype.Component;

@Component(PaymentType.PAYMENT)
public class PaymentTypeHandler implements TypeHandler {
    @Override
    public BigDecimal calculateAmount(Rental rental) {
        long numberOfDays = ChronoUnit.DAYS.between(
                rental.getRentalDate(), rental.getReturnDate()
        );

        return rental.getCar().getDailyFee()
                .multiply(BigDecimal.valueOf(numberOfDays));
    }
}
