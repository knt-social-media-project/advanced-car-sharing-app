package com.nure.knt.benders.advanced.car.sharing.strategy.payment.handlers.type;

import com.nure.knt.benders.advanced.car.sharing.model.Rental;
import com.nure.knt.benders.advanced.car.sharing.strategy.payment.TypeHandler;
import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;
import org.springframework.stereotype.Component;

@Component(PaymentType.FINE)
public class FineTypeHandler implements TypeHandler {
    private static final Double FINE_MULTIPLIER = 1.5;

    @Override
    public BigDecimal calculateAmount(Rental rental) {
        long numberOfDays = ChronoUnit.DAYS.between(
                rental.getReturnDate(), rental.getActualReturnDate()
        );

        return rental.getCar().getDailyFee()
                .multiply(BigDecimal.valueOf(numberOfDays))
                .multiply(BigDecimal.valueOf(FINE_MULTIPLIER));
    }
}
