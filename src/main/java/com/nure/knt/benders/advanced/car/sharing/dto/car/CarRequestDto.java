package com.nure.knt.benders.advanced.car.sharing.dto.car;

import com.nure.knt.benders.advanced.car.sharing.model.Car;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import jakarta.validation.constraints.Size;
import java.math.BigDecimal;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CarRequestDto {
    @NotNull(message = "name cannot be null")
    @Size(max = 255, message = "name cannot be greater than 255 characters")
    private String brand;

    @NotNull(message = "name cannot be null")
    @Size(max = 255, message = "name cannot be greater than 255 characters")
    private String model;

    @NotNull
    private Car.Type type;

    @Min(0)
    private int inventory;

    @NotNull(message = "cannot be null")
    @Positive
    private BigDecimal dailyFee;
}
