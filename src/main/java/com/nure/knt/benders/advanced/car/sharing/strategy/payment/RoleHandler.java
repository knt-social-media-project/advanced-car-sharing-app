package com.nure.knt.benders.advanced.car.sharing.strategy.payment;

import com.nure.knt.benders.advanced.car.sharing.model.Payment;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RoleHandler {
    Page<Payment> getPayments(Long userId, Pageable pageable);
}
