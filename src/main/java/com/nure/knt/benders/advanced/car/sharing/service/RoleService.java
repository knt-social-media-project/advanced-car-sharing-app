package com.nure.knt.benders.advanced.car.sharing.service;

import com.nure.knt.benders.advanced.car.sharing.dto.role.RoleResponseDto;
import java.util.List;
import org.springframework.data.domain.Pageable;

public interface RoleService {
    List<RoleResponseDto> getRoles(Pageable pageable);
}
