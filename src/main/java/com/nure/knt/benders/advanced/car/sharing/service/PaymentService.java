package com.nure.knt.benders.advanced.car.sharing.service;

import com.nure.knt.benders.advanced.car.sharing.dto.payment.PaymentRequestDto;
import com.nure.knt.benders.advanced.car.sharing.dto.payment.PaymentResponseDto;
import java.util.List;
import org.springframework.data.domain.Pageable;

public interface PaymentService {
    List<PaymentResponseDto> getAll(Long userId, Pageable pageable);

    PaymentResponseDto createPaymentSession(PaymentRequestDto paymentRequestDto);

    String setSuccessfulPayment(String sessionId);

    String setCanceledPayment(String sessionId);
}
