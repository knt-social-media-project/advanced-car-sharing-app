package com.nure.knt.benders.advanced.car.sharing.exception;

public class RentalReturnException extends RuntimeException {
    public RentalReturnException(String message) {
        super(message);
    }
}
