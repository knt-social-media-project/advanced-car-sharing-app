package com.nure.knt.benders.advanced.car.sharing.dto.role;

import lombok.Data;

@Data
public class RoleResponseDto {
    private Long id;
    private String role;
}
