package com.nure.knt.benders.advanced.car.sharing.service.impl;

import com.nure.knt.benders.advanced.car.sharing.service.NotificationService;
import com.nure.knt.benders.advanced.car.sharing.telegram.TelegramBot;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class TelegramNotificationService implements NotificationService {
    private final TelegramBot telegramBot;

    @Override
    public void sendNotification(Long userChatId, String message) {
        telegramBot.sendMessage(userChatId, message);
    }
}
