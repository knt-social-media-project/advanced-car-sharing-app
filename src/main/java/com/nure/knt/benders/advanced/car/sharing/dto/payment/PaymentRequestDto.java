package com.nure.knt.benders.advanced.car.sharing.dto.payment;

import com.nure.knt.benders.advanced.car.sharing.model.Payment;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Positive;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PaymentRequestDto {
    @NotNull(message = "can't be null")
    @Positive(message = "must be greater than 0")
    private Long rentalId;
    @NotNull(message = "can't be null")
    private Payment.Type type;
}
