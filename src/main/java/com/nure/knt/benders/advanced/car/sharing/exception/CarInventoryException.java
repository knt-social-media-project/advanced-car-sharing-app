package com.nure.knt.benders.advanced.car.sharing.exception;

public class CarInventoryException extends RuntimeException {
    public CarInventoryException(String message) {
        super(message);
    }
}
