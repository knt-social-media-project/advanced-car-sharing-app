package com.nure.knt.benders.advanced.car.sharing.service;

public interface NotificationService {
    void sendNotification(Long userChatId, String message);
}
