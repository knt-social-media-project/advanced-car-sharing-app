package com.nure.knt.benders.advanced.car.sharing.strategy.payment.handlers.roles;

import com.nure.knt.benders.advanced.car.sharing.model.Payment;
import com.nure.knt.benders.advanced.car.sharing.model.Rental;
import com.nure.knt.benders.advanced.car.sharing.repository.PaymentRepository;
import com.nure.knt.benders.advanced.car.sharing.repository.RentalRepository;
import com.nure.knt.benders.advanced.car.sharing.strategy.payment.RoleHandler;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component(RoleType.CUSTOMER)
@RequiredArgsConstructor
public class CustomerRoleHandler implements RoleHandler {
    private final RentalRepository rentalRepository;
    private final PaymentRepository paymentRepository;

    @Override
    public Page<Payment> getPayments(Long userId, Pageable pageable) {
        List<Rental> rentals = rentalRepository.findAllByUserId(userId);

        return paymentRepository.findAllByRentalIn(rentals, pageable);
    }
}
