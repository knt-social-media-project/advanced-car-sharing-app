package com.nure.knt.benders.advanced.car.sharing.service.impl;

import com.nure.knt.benders.advanced.car.sharing.dto.role.RoleResponseDto;
import com.nure.knt.benders.advanced.car.sharing.mapper.RoleMapper;
import com.nure.knt.benders.advanced.car.sharing.repository.RoleRepository;
import com.nure.knt.benders.advanced.car.sharing.service.RoleService;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class RoleServiceImpl implements RoleService {
    private final RoleRepository roleRepository;
    private final RoleMapper roleMapper;

    @Override
    public List<RoleResponseDto> getRoles(Pageable pageable) {
        return roleRepository.findAll(pageable).stream()
                .map(roleMapper::toDto)
                .toList();
    }
}
