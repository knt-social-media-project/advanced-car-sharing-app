package com.nure.knt.benders.advanced.car.sharing.exception;

public class PendingPaymentExistsException extends RuntimeException {
    public PendingPaymentExistsException(String message) {
        super(message);
    }
}
