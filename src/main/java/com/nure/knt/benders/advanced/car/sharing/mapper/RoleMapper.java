package com.nure.knt.benders.advanced.car.sharing.mapper;

import com.nure.knt.benders.advanced.car.sharing.config.MapperConfig;
import com.nure.knt.benders.advanced.car.sharing.dto.role.RoleResponseDto;
import com.nure.knt.benders.advanced.car.sharing.model.Role;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapperConfig.class)
public interface RoleMapper {
    @Mapping(target = "role", source = "roleName")
    RoleResponseDto toDto(Role role);
}
