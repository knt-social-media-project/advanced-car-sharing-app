package com.nure.knt.benders.advanced.car.sharing.dto.user.email;

public record UserUpdateEmailResponseDto(
         Long id,
         String email
) {
}
