package com.nure.knt.benders.advanced.car.sharing.exception;

public class StripeProcessingException extends RuntimeException {
    public StripeProcessingException(String message, Throwable cause) {
        super(message, cause);
    }
}
