package com.nure.knt.benders.advanced.car.sharing.mapper;

import com.nure.knt.benders.advanced.car.sharing.config.MapperConfig;
import com.nure.knt.benders.advanced.car.sharing.dto.car.CarRequestDto;
import com.nure.knt.benders.advanced.car.sharing.dto.car.CarResponseDto;
import com.nure.knt.benders.advanced.car.sharing.model.Car;
import org.mapstruct.Mapper;

@Mapper(config = MapperConfig.class)
public interface CarMapper {
    CarResponseDto toDto(Car car);

    Car toModel(CarRequestDto requestDto);
}
