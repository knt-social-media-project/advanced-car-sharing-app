package com.nure.knt.benders.advanced.car.sharing.validation;

import com.nure.knt.benders.advanced.car.sharing.dto.user.password.UserUpdatePasswordRequestDto;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import java.util.Objects;

public class PasswordMatchesValidator implements ConstraintValidator<FieldMatchChangePassword,
        UserUpdatePasswordRequestDto> {
    @Override
    public boolean isValid(UserUpdatePasswordRequestDto requestDto,
                           ConstraintValidatorContext constraintValidatorContext) {
        return requestDto.getPassword() != null
                && Objects.equals(requestDto.getPassword(), requestDto.getRepeatPassword());
    }
}
