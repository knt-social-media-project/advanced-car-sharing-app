package com.nure.knt.benders.advanced.car.sharing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AdvancedCarSharingApplication {

    public static void main(String[] args) {
        SpringApplication.run(AdvancedCarSharingApplication.class, args);
    }

}
