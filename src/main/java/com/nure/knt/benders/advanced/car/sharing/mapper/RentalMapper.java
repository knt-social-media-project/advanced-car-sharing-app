package com.nure.knt.benders.advanced.car.sharing.mapper;

import com.nure.knt.benders.advanced.car.sharing.config.MapperConfig;
import com.nure.knt.benders.advanced.car.sharing.dto.rental.RentalResponseDto;
import com.nure.knt.benders.advanced.car.sharing.model.Rental;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(config = MapperConfig.class)
public interface RentalMapper {
    @Mapping(target = "carId", source = "car.id")
    @Mapping(target = "userId", source = "user.id")
    RentalResponseDto toDto(Rental rental);
}
