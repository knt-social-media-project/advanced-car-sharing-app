package com.nure.knt.benders.advanced.car.sharing.strategy.payment;

import com.nure.knt.benders.advanced.car.sharing.model.Rental;
import java.math.BigDecimal;

public interface TypeHandler {
    BigDecimal calculateAmount(Rental rental);
}
