package com.nure.knt.benders.advanced.car.sharing.repository;

import com.nure.knt.benders.advanced.car.sharing.model.Rental;
import java.util.List;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RentalRepository extends JpaRepository<Rental, Long> {

    @EntityGraph(attributePaths = {"car", "user"})
    Page<Rental> findAllByUserIdAndIsActive(Long userId, Boolean isActive, Pageable pageable);

    @EntityGraph(attributePaths = {"car", "user"})
    List<Rental> findAllByIsActive(Boolean isActive);

    @EntityGraph(attributePaths = {"car", "user"})
    Optional<Rental> findByIdAndUserId(Long id, Long userId);

    List<Rental> findAllByUserId(Long userId);

    @EntityGraph(attributePaths = {"car", "user"})
    Optional<Rental> findById(Long id);
}
