package com.nure.knt.benders.advanced.car.sharing.dto.user.profile;

import lombok.Data;

@Data
public class UserWithNameAndLastNameRequestDto {
    private String firstName;
    private String lastName;
}
