package com.nure.knt.benders.advanced.car.sharing.service;

import com.nure.knt.benders.advanced.car.sharing.dto.role.RoleUpdateForUserRequestDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.UserResponseDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.email.UserUpdateEmailRequestDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.email.UserUpdateEmailResponseDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.password.UserUpdatePasswordRequestDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.profile.UserWithNameAndLastNameRequestDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.profile.UserWithNameAndLastNameResponseDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.registration.UserRegistrationRequestDto;
import com.nure.knt.benders.advanced.car.sharing.dto.user.role.UserWithRoleResponseDto;
import com.nure.knt.benders.advanced.car.sharing.exception.RegistrationException;

public interface UserService {
    UserResponseDto register(UserRegistrationRequestDto requestDto) throws RegistrationException;

    UserWithRoleResponseDto updateRole(Long userId, RoleUpdateForUserRequestDto requestDto);

    UserUpdateEmailResponseDto updateEmail(Long userId, UserUpdateEmailRequestDto requestDto);

    UserResponseDto getProfile(String email);

    UserResponseDto updatePassword(Long userId, UserUpdatePasswordRequestDto requestDto);

    UserWithNameAndLastNameResponseDto updateProfile(Long id,
                                                     UserWithNameAndLastNameRequestDto requestDto);
}
