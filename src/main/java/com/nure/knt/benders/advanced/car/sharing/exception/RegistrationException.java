package com.nure.knt.benders.advanced.car.sharing.exception;

public class RegistrationException extends Exception {
    public RegistrationException(String message) {
        super(message);
    }
}
