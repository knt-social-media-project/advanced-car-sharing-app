package com.nure.knt.benders.advanced.car.sharing.repository;

import com.nure.knt.benders.advanced.car.sharing.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {
    Role findByRoleName(Role.RoleName roleName);
}
