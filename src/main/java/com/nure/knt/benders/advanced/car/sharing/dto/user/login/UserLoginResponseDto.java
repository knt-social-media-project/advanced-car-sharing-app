package com.nure.knt.benders.advanced.car.sharing.dto.user.login;

public record UserLoginResponseDto(String token) {
}
