package com.nure.knt.benders.advanced.car.sharing.controller;

import com.nure.knt.benders.advanced.car.sharing.dto.role.RoleResponseDto;
import com.nure.knt.benders.advanced.car.sharing.service.RoleService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Tag(name = "Users management", description = "Endpoints for managing ssers")
@RequiredArgsConstructor
@RestController
@RequestMapping(value = "/roles")
public class RoleController {
    private final RoleService roleService;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping
    @Operation(summary = "Get all roles",
            description = "The admin can get a list of roles by the following parameters")
    public List<RoleResponseDto> getRoles(Pageable pageable) {
        return roleService.getRoles(pageable);
    }
}

