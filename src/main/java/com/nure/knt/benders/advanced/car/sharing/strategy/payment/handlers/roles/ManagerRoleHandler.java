package com.nure.knt.benders.advanced.car.sharing.strategy.payment.handlers.roles;

import com.nure.knt.benders.advanced.car.sharing.model.Payment;
import com.nure.knt.benders.advanced.car.sharing.repository.PaymentRepository;
import com.nure.knt.benders.advanced.car.sharing.strategy.payment.RoleHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

@Component(RoleType.MANAGER)
@RequiredArgsConstructor
public class ManagerRoleHandler implements RoleHandler {
    private final PaymentRepository paymentRepository;

    @Override
    public Page<Payment> getPayments(Long userId, Pageable pageable) {
        return paymentRepository.findAll(pageable);
    }
}
