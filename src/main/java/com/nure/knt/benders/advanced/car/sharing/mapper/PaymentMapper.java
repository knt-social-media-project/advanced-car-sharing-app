package com.nure.knt.benders.advanced.car.sharing.mapper;

import com.nure.knt.benders.advanced.car.sharing.config.MapperConfig;
import com.nure.knt.benders.advanced.car.sharing.dto.payment.PaymentRequestDto;
import com.nure.knt.benders.advanced.car.sharing.dto.payment.PaymentResponseDto;
import com.nure.knt.benders.advanced.car.sharing.model.Payment;
import com.nure.knt.benders.advanced.car.sharing.model.Rental;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(config = MapperConfig.class)
public interface PaymentMapper {
    @Mapping(target = "rentalId", source = "rental.id")
    PaymentResponseDto toDto(Payment payment);

    @Mapping(target = "rental", source = "rentalId", qualifiedByName = "rentalFromId")
    Payment toModel(PaymentRequestDto paymentRequestDto);

    @Named("rentalFromId")
    default Rental rentalFromId(Long rentalId) {
        return new Rental().setId(rentalId);
    }
}
