package com.nure.knt.benders.advanced.car.sharing.exception;

public class PickHandlerException extends RuntimeException {
    public PickHandlerException(String message) {
        super(message);
    }
}
