package com.nure.knt.benders.advanced.car.sharing.strategy.payment.handlers.type;

public class PaymentType {
    public static final String PAYMENT = "PAYMENT";
    public static final String FINE = "FINE";
}
