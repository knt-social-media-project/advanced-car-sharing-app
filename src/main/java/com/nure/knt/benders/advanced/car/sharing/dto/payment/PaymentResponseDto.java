package com.nure.knt.benders.advanced.car.sharing.dto.payment;

import com.nure.knt.benders.advanced.car.sharing.model.Payment;
import java.math.BigDecimal;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class PaymentResponseDto {
    private Long id;
    private Payment.Status status;
    private Payment.Type type;
    private Long rentalId;
    private String session;
    private String sessionId;
    private BigDecimal amount;
}
