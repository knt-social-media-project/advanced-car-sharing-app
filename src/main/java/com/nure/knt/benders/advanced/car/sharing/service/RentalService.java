package com.nure.knt.benders.advanced.car.sharing.service;

import com.nure.knt.benders.advanced.car.sharing.dto.rental.RentalRequestDto;
import com.nure.knt.benders.advanced.car.sharing.dto.rental.RentalResponseDto;
import java.util.List;
import org.springframework.data.domain.Pageable;

public interface RentalService {
    RentalResponseDto save(RentalRequestDto requestDto, Long userId);

    List<RentalResponseDto> getListByUserIdAndIsActiveStatus(
            Long userId,
            boolean isActive,
            Pageable pageable
    );

    RentalResponseDto findByIdAndUserId(Long id, Long userId);

    RentalResponseDto update(Long id);
}
